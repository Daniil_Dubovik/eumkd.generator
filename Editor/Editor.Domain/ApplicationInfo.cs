﻿namespace Editor.Domain
{
    public static class ApplicationInfo
    {
        public static string Name { get; }
        public static string Version { get; }
        public static string Author { get; }
        public static string Feedback { get; }
        public static string AdditionalInfo { get; }

        static ApplicationInfo()
        {
            Name = "EUMK Generator";
            Version = "1.0";
            Author = "Daniil Dubovik";
            Feedback = "daniil.dubovik@gmail.com";
            AdditionalInfo = "BSUIR 2017";
        }
    }
}
