﻿namespace Editor.Domain.Entities.Tags
{
    public class TextTag : Tag
    {
        public TextTag()
        {
            TagType = TagType.Text;
        }
    }
}

