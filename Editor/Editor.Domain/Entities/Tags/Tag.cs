﻿namespace Editor.Domain.Entities.Tags
{
    public class Tag
    {
        public string Name { get; set; }
        public TagType TagType { get; protected set; } = TagType.None;
        public string Content { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
