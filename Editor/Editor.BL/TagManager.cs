﻿using System.Collections.Generic;
using System.Linq;
using Editor.DAL;
using Editor.Domain.Entities.Tags;

namespace Editor.BL
{
    public class TagManager
    {
        private readonly IRepository<Tag> _repository;

        public TagManager(IRepository<Tag> repository)
        {
            _repository = repository;
        }

        public List<Tag> GetAllTags()
        {
           return _repository.Read().ToList();
        }

        public void SaveAllTags(List<Tag> tags)
        {
            foreach (var tag in tags)
            {
                _repository.Update(tag);
            }
        }
    }
}
