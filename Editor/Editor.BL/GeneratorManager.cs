﻿using System;
using System.IO;
using System.Threading;
using Editor.DAL.Repositories;
using Editor.Domain.Entities.Tags;

namespace Editor.BL
{
    public class GeneratorManager
    {
        public void AsyncGenerateUsingTemplate(string path, Action<object> finalAction, Action<object> failAction)
        {
            var asyncGenerationThread = new Thread(() => AsyncGenerate(path, finalAction, failAction));
            asyncGenerationThread.Start();
        }

        private void AsyncGenerate(string path, Action<object> finalAction, Action<object> failAction)
        {
            try
            {
                CopyDirectory(HtmlRepository<Tag>.TemplatePath, path, true);
            }
            catch (Exception)
            {
                failAction.Invoke(path);
                return;
            }
            
            finalAction.Invoke(path);
        }

        private void CopyDirectory(string source, string destination, bool isCopySubDirectories)
        {
            DirectoryInfo sourceDirectory = new DirectoryInfo(source);

            if (!sourceDirectory.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Template does not exist or could not be found: "
                    + source);
            }

            DirectoryInfo[] directories = sourceDirectory.GetDirectories();

            if (!System.IO.Directory.Exists(destination))
            {
                System.IO.Directory.CreateDirectory(destination);
            }

            FileInfo[] files = sourceDirectory.GetFiles();
            foreach (FileInfo file in files)
            {
                string filePath = Path.Combine(destination, file.Name);

                file.CopyTo(filePath, false);
            }

            if (isCopySubDirectories)
            {
                foreach (DirectoryInfo subDirectory in directories)
                {
                    string subDirectoryPath = Path.Combine(destination, subDirectory.Name);

                    CopyDirectory(subDirectory.FullName, subDirectoryPath, true);
                }
            }
        }
    }
}
