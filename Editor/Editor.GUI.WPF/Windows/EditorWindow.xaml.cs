﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Editor.BL;
using Editor.DAL.Repositories;
using Editor.Domain;
using Editor.Domain.Entities.Tags;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;
using TagType = Editor.Domain.Entities.Tags.TagType;

namespace Editor.GUI.WPF.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class EditorWindow
    {
        private bool _isEUMKLoaded;

        public EditorWindow()
        {
            InitializeComponent();

            _isEUMKLoaded = false;

            WindowStyle = WindowStyle.SingleBorderWindow;
            ResizeMode = ResizeMode.NoResize;

            Title = ApplicationInfo.Name;
        }

        private void NewFileMenuItem_Click(object sender, RoutedEventArgs e)
        {
            using (var folderBrowserDialog = new FolderBrowserDialog())
            {
                DialogResult result = folderBrowserDialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                {
                    var generatorManager = new GeneratorManager();
                    generatorManager.AsyncGenerateUsingTemplate(folderBrowserDialog.SelectedPath, 
                        NotificateAboutSuccessfulGeneration, NotificateAboutUnsuccessfulGeneration);
                }
            }
        }

        private void NotificateAboutSuccessfulGeneration(object path)
        {
            path = path as string;

            MessageBox.Show($"EUMK has created!\nPath:{path}", "Done!", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void NotificateAboutUnsuccessfulGeneration(object path)
        {
            path = path as string;

            MessageBox.Show($"Generation of EUMK failed.\nPath:{path}", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void OpenFileMenuItem_Click(object sender, RoutedEventArgs e)
        {
            using (var folderBrowserDialog = new FolderBrowserDialog())
            {
                folderBrowserDialog.ShowNewFolderButton = false;
                DialogResult result = folderBrowserDialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                {
                    TagsListBox.Items.Clear();
                    TextTagTextBox.Text = string.Empty;
                    TextTagGrid.Visibility = Visibility.Hidden;

                    var tagManager = new TagManager(new HtmlRepository<Tag>(folderBrowserDialog.SelectedPath));
                    List<Tag> tags = tagManager.GetAllTags();

                    if (tags.Count == 0)
                    {
                        _isEUMKLoaded = false;
                        return;
                    }

                    foreach (var tag in tags)
                    {
                        TagsListBox.Items.Add(tag);
                    }

                    _isEUMKLoaded = true;
                }
            }
        }

        private void TagsListBox_ClickOnItem(object sender, MouseButtonEventArgs e)
        {
            var tagName = TagsListBox.Items[TagsListBox.SelectedIndex] as Tag;

            if (tagName == null)
                throw new NullReferenceException();

            TextTagTextBox.Text = tagName.Content;

            switch (tagName.TagType)
            {
                case TagType.Text:
                    TextTagGrid.Visibility = Visibility.Visible;
                    break;

                case TagType.None:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void TextTagSaveButton_Click(object sender, RoutedEventArgs e)
        {
            var tagName = TagsListBox.Items[TagsListBox.SelectedIndex] as Tag;

            if (tagName == null)
                throw new NullReferenceException();

            tagName.Content = TextTagTextBox.Text;
        }

        private void TagsSaveAllTags_Click(object sender, RoutedEventArgs e)
        {
            if (!_isEUMKLoaded)
            {
                return;
            }

            var tagManager = new TagManager(new HtmlRepository<Tag>());
            var tags = new List<Tag>();

            foreach (var item in TagsListBox.Items)
            {
                tags.Add((Tag)item);
            }

            tagManager.SaveAllTags(tags);
        }

        private void AboutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show($"{ApplicationInfo.Name} {ApplicationInfo.Version}.\n\n{ApplicationInfo.Author}\n{ApplicationInfo.Feedback}\n\n{ApplicationInfo.AdditionalInfo}", "About", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void EditorWindow_OnClosing(object sender, CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to close this window?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}

