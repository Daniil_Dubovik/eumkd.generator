﻿#pragma checksum "..\..\..\Windows\GreetingWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F64C81F1AAC4DFEB9280C6723A70D825"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Editor.GUI.WPF;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Editor.GUI.WPF {
    
    
    /// <summary>
    /// GreetingWindow
    /// </summary>
    public partial class StartWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OpenProjectButton;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CreateProjectButton;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RecentProjectButton;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid RecentProjectsGrid;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid NewProjectsGrid;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image EmblemImage;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label EmblemLabel;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddEmblemButton;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ThemeLabel;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ThemeTextBox;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label SpecialtyLabel;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SpecialtyTextBox;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label FooterLabel;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FooterTextBox;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Windows\GreetingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AcceptCreatingNewProjectButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Editor.GUI.WPF;component/windows/greetingwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Windows\GreetingWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OpenProjectButton = ((System.Windows.Controls.Button)(target));
            return;
            case 2:
            this.CreateProjectButton = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\..\Windows\GreetingWindow.xaml"
            this.CreateProjectButton.Click += new System.Windows.RoutedEventHandler(this.CreateProjectButton_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.RecentProjectButton = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\..\Windows\GreetingWindow.xaml"
            this.RecentProjectButton.Click += new System.Windows.RoutedEventHandler(this.RecentProjectButton_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.RecentProjectsGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.NewProjectsGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.EmblemImage = ((System.Windows.Controls.Image)(target));
            return;
            case 7:
            this.EmblemLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.AddEmblemButton = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\..\Windows\GreetingWindow.xaml"
            this.AddEmblemButton.Click += new System.Windows.RoutedEventHandler(this.AddEmblemButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.ThemeLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.ThemeTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.SpecialtyLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.SpecialtyTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.FooterLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.FooterTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.AcceptCreatingNewProjectButton = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\Windows\GreetingWindow.xaml"
            this.AcceptCreatingNewProjectButton.Click += new System.Windows.RoutedEventHandler(this.AcceptCreatingNewProjectButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

