﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Editor.Domain.Entities.Tags;
using HtmlAgilityPack;

namespace Editor.DAL.Repositories
{
    public class HtmlRepository<T> : IRepository<T> where T : Tag
    {
        public static readonly string TemplatePath = $@"{Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent?.Parent?.FullName)}\Editor.DAL\Templates\{ConfigurationManager.AppSettings["TemplateName"]}\";
        
        private readonly HtmlAgilityPack.HtmlDocument _htmlDocument = new HtmlDocument();
        private static string _path;
        private readonly HtmlMapper _htmlMapper = new HtmlMapper();

        public HtmlRepository(string path)
        {
            _path = path;
        }

        public HtmlRepository()
        {
        }

        public void Create(T obj)
        {
        }

        public ICollection<T> Read(params string[] includes)
        {
            var files = new List<string>();
            GetPathsFromDirectory(_path, files);

            List<string> htmFilePaths =
            (from file in files let extension = Path.GetExtension(file) where extension == ".htm" || extension == ".html" select file).ToList();

            var tags = new List<Tag>();

            foreach (var htmlFilePath in htmFilePaths)
            {
                _htmlDocument.Load(htmlFilePath);

                var textNodes = _htmlDocument.DocumentNode.SelectNodes($"//comment()[contains(.,'{ConfigurationManager.AppSettings["TagSpecialSymbol"]}t_')]");

                if (textNodes == null) continue;

                if (textNodes.Any())
                {
                    foreach (var node in textNodes)
                    {
                        var tag = _htmlMapper.ConvertToTag(node.NextSibling);

                        if (tag == null) continue;

                        if (tags.Find(x => x.Name == tag.Name) == null)
                        {
                            tags.Add(tag);
                        }
                    }
                }
            }

            return (ICollection<T>) tags;
        }

        public void Update(T obj)
        {
            var files = new List<string>();
            GetPathsFromDirectory(_path, files);

            List<string> htmFilePaths =
            (from file in files let extension = Path.GetExtension(file) where extension == ".htm" || extension == ".html" select file).ToList();

            var tags = new HashSet<Tag>();

            foreach (var htmlFilePath in htmFilePaths)
            {
                _htmlDocument.Load(htmlFilePath);

                var tag = $"{ConfigurationManager.AppSettings["TagSpecialSymbol"]}t_{obj.Name}";
                var node = _htmlDocument.DocumentNode.SelectSingleNode($"//comment()[contains(.,'{tag}')]");

                node?.NextSibling.ParentNode.ReplaceChild(HtmlNode.CreateNode($"{obj.Content}"), node.NextSibling);

                _htmlDocument.Save(htmlFilePath);
            }
        }

        public void Delete(T obj)
        {
        }

        public void GetPathsFromDirectory(string directory, List<string> paths)
        {
            try
            {
                string[] filePaths = Directory.GetFiles(directory);

                foreach (string filePath in filePaths)
                {
                    paths.Add(filePath);
                }

                string[] subDirectories = Directory.GetDirectories(directory);

                foreach (string subDirectory in subDirectories)
                {
                    GetPathsFromDirectory(subDirectory, paths);
                }   
            }
            catch (Exception e)
            {
                // ignored
            }
        }
    }
}
