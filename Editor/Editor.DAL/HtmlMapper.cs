﻿using System.Configuration;
using System.Text;
using Editor.Domain.Entities.Tags;
using HtmlAgilityPack;

namespace Editor.DAL
{
    public class HtmlMapper
    {
        public Tag ConvertToTag(HtmlNode node)
        {
            switch (node.NodeType)
            {
                case HtmlNodeType.Text:
                    return CreateTextTag(node);
            }

            return null;
        }

        private TextTag CreateTextTag(HtmlNode node)
        {
            var stringBuilder = new StringBuilder(node.PreviousSibling.InnerText);

            stringBuilder.Replace($"<!--{ConfigurationManager.AppSettings["TagSpecialSymbol"]}t_", string.Empty);
            stringBuilder.Replace("-->", string.Empty);

            var textTag = new TextTag
            {
                Name = stringBuilder.ToString(),
                Content = node.InnerText
            };

            if (textTag.Content == " ")
            {
                textTag.Content = string.Empty;
            }

            return textTag;
        }
    }
}
