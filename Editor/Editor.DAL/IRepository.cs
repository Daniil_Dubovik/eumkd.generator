﻿using System.Collections.Generic;

namespace Editor.DAL
{
    public interface IRepository<T>
    {
        void Create(T obj);
        ICollection<T> Read(params string[] includes);
        void Update(T obj);
        void Delete(T obj);
    }
}
